##MensaApp##
Die MensaApp gibt eine simple und übersichtliche Echtzeitbewertung von Mensaspeisen und das Anzeigen der Bewertungen und Kommentare anderer Nutzer wieder.

##Wie importiert/installiert man das Projekt?##
Laden Sie sich die APK herunter, die Sie auf Ihrem Smartphone anschließend installieren können.  
*Hinweis: Diese Software ist nur für Android Geräte kompatibel.*  
  
Zum Importieren des Projekts in Android Studio geben Sie folgenden Befehl ein:  
```
git clone https://nutzername@bitbucket.org/aass16team11/htw.mensaapp.git
```

##Beispiele/Features##
Screen zum Anzeigen der Speiseliste:  
![alt text](https://bytebucket.org/Edanxx/readmemensaapp/raw/55d8b09b13ae92f48f3485d872ea33a6a29f1207/mensaappsc2.png)

Screen zum Bewerten eines Gerichts:  
![alt text](https://bytebucket.org/Edanxx/readmemensaapp/raw/65b90b85764ba720e955fe2353aee1a7f61afe8b/mensaappsc.png)  
  
Weitere Features beinhalten:  
- Anzeigen von Bewertungen anderer Nutzer zu einer Speise  
- Anzeigen von Kommentaren anderer Nutzer zu einer Speise

##Kontaktinformation##
SS16 - Team 11